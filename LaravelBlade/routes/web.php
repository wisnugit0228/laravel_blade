<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TableController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'halutama']);

Route::get('/register', [AuthController::class, 'reg']);

Route::get('/welcome', [AuthController::class, 'wel']);

route::get('/main', function(){
    return view('layout.main');
});

Route::get('/table', [TableController::class, 'table']);

Route::get('/data-table', [TableController::class, 'datatable']);